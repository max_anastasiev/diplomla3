import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.Login;
import test.StartPage;


@RunWith(Suite.class)
@Suite.SuiteClasses
        (value = {
                StartPage.class,
                Login.class,
//                RestTest.class,
        })


    public class TestSuit {
        @BeforeClass
        public static void setUp() {
        }

        @AfterClass
        public static void close() {

        }
}

