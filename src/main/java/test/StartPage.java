package test;

import core.WebDriverSettings;
import org.junit.Ignore;
import org.junit.Test;

public class StartPage extends WebDriverSettings {


    @Test
    public void SubmitNews(){

        startPagePOM.getTermsCheckBox();
        startPagePOM.getSubmitNews();
    }

    @Ignore
    @Test
    public void goToLogin(){

        startPagePOM.goToLogin();
    }
}
