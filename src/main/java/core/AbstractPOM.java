package core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;

public abstract class AbstractPOM {
	public WebDriver driver;
	public WebDriverWait wait;
	public AbstractPOM(WebDriver driver) {
		wait = new WebDriverWait(driver, 10, 500);
		this.driver = driver;
		PageFactory.initElements(new HtmlElementDecorator(driver), this);
	}

}
