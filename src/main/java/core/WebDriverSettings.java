package core;

import java.util.concurrent.TimeUnit;

import objects.StartPagePOM;
import objects.LoginPagePOM;
import objects.SubmitAsFreelancerPOM;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import rules.ScreenshotTestRule;


public class WebDriverSettings {
    private static final Logger log = LogManager.getLogger(WebDriverSettings.class);
    public static WebDriver driver = new ChromeDriver();

    public WebDriverWait wait;

    private String url = "http://gin.cs1.soft-tehnica.com/user";

    public SubmitAsFreelancerPOM submitAsFreelancerPOM = new SubmitAsFreelancerPOM(driver);
    public LoginPagePOM loginPagePOM = new LoginPagePOM(driver);
    public StartPagePOM startPagePOM = new StartPagePOM(driver);
    public Delay delay = new Delay();

    @Rule
    public ScreenshotTestRule screenshotTestRule = new ScreenshotTestRule(driver);

    @Before
    @Step("SetUp")
    public void setUp() {
        log.info("Open browser and access - " + url);
        log.info(getClass().getSimpleName() + " test is in progress");
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 10, 500);
        driver.get(url);
        log.info("Check page title");
        Assert.assertEquals("Welcome to demo.t4hub | demo.t4hub", driver.getTitle());

    }


    @After
    @Step("Close browser")
    public void close() {
        log.info("Close browser at page - " + driver.getCurrentUrl());
    }
}
