package core;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Delay {
	private static final Logger log = LogManager.getLogger(Delay.class);
	public void delayTime(int delay){
		try {
			log.info("Thread sleep to " + delay +" milesceconds");
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
