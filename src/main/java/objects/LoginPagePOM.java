package objects;

import core.AbstractPOM;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

public class LoginPagePOM extends AbstractPOM {

    @FindBy(xpath = "//*[@id=\"edit-name\"]")
    public TextInput username;

    @FindBy(xpath = "//*[@id=\"edit-pass\"]")
    public TextInput password;

    @FindBy(xpath = "//*[@id=\"edit-submit\"]")
    public Button login;


    public LoginPagePOM(WebDriver driver) {
        super(driver);
    }

    @Step("Access system")
    public void Login(String usr, String pass){

        username.clear();
        username.sendKeys(usr);
        password.clear();
        password.sendKeys(pass);
        login.click();
    }
}
