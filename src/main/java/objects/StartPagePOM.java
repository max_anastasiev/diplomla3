package objects;

import core.AbstractPOM;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.CheckBox;


public class StartPagePOM extends AbstractPOM{

    private static final Logger log = LogManager.getLogger(StartPagePOM.class);


    @FindBy(xpath = "//*[@id=\"terms\"]")
    public CheckBox termscheckbox;

    @FindBy(xpath = "//*[@id=\"block-block-1\"]/p[2]/a")
    public Button submitnews;

    @FindBy(xpath = "//*[@id=\"block-block-1\"]/div/div[2]/a")
    public Button loginbutton;

    @FindBy(xpath = "//*[@id=\"block-block-1\"]/div/div[1]/a")
    public Button contactbutton;

    public StartPagePOM(WebDriver driver) {
        super(driver);
    }

    @Step("Click Read and Agree Check Box")
    public void getTermsCheckBox(){
        log.info("Select - " + termscheckbox.getLabelText());
        termscheckbox.select();
    }

    @Step
    public void getSubmitNews(){
        log.info("Click to - " + submitnews.getWrappedElement().getText());
        submitnews.click();
    }

    @Step("Go to page Login")
    public void goToLogin(){
        loginbutton.click();
    }
}
