package objects;

import core.AbstractPOM;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.CheckBox;
import ru.yandex.qatools.htmlelements.element.Select;
import ru.yandex.qatools.htmlelements.element.TextInput;
import rules.GenerateData;


public class SubmitAsFreelancerPOM extends AbstractPOM{


    @FindBy(xpath = "//*[@id=\"edit-field-new-media-type-und\"]/div[1]/label/i")
    public Button mediatype;

    @FindBy(xpath = "//*[@id=\"edit-field-slug-text-und-0-value\"]")
    public TextInput slug;

    @FindBy(xpath = "//*[@id=\"edit-title\"]")
    public TextInput headline;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-text-source-und-form-field-text-und-0-summary\"]")
    public TextInput summary;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-text-source-und-form-field-text-und-0-value\"]")
    public TextInput story;



    //Notes bar
    @FindBy(xpath = "//*[@id=\"notes-tablist\"]/li[1]/a")
    public Button legalnotesbutton;

    @FindBy(xpath = "//*[@id=\"edit-field-legal-notes-news-und-0-value\"]")
    public TextInput legalnotes;

    @FindBy(xpath = "//*[@id=\"notes-tablist\"]/li[2]/a")
    public Button backgroundinfobutton;

    @FindBy(xpath = "//*[@id=\"edit-field-bg-info-notes-news-und-0-value\"]")
    public TextInput backgroundinfo;

    @FindBy(xpath = "//*[@id=\"notes-tablist\"]/li[3]/a")
    public Button alreadyoutbutton;

    @FindBy(xpath = "//*[@id=\"edit-field-already-out-notes-news-und-0-value\"]")
    public TextInput alreadyout;

    @FindBy(xpath = "//*[@id=\"notes-tablist\"]/li[4]/a")
    public Button archivebutton;

    @FindBy(xpath = "//*[@id=\"edit-field-archive-notes-news-und-0-value\"]")
    public TextInput archive;

    @FindBy(xpath = "//*[@id=\"notes-tablist\"]/li[5]/a")
    public Button suggestsendingbutton;

    @FindBy(xpath = "//*[@id=\"edit-field-suggest-sending-news-und-0-value\"]")
    public TextInput suggestsending;



    //Original source
    @FindBy(name = "files[field_inline_text_source_und_form_field_source_material_und_0_field_sm_file_und_0]")
    public WebElement sourcechoosefile;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-text-source-und-form-field-source-material-und-0-field-sm-file-und-0-upload-button\"]")
    public Button sourceuploadfile;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-text-source-und-form-field-source-material-und-0-remove-button--3\"]")
    public Button deleteuploadedfile;

    @FindBy(xpath = "//*[@id=\"edit-field-inline-text-source-und-form-field-source-material-und-add-more--5\"]")
    public Button addanotheritem;


//    //Add picture
//    @FindBy(xpath = "//*[@id=\"edit-field-inline-pictures-source-und-form-field-news-link-und-0-url\"]")
//    public WebElement picturesource;
//
//    @FindBy(xpath = "//*[@id=\"edit-field-inline-pictures-source-und-form-field-news-copyright-und-0-value\"]")
//    public WebElement picturecopyright;
//
//    @FindBy(xpath = "//*[@id=\"edit-field-inline-pictures-source-und-form-field-picture-description-und-0-value\"]")
//    public WebElement picturecaption;
//
//    @FindBy(xpath = "//*[@id=\"edit-field-inline-pictures-source-und-form-field-img-news-original-und-0-upload\"]")
//    public WebElement picturechoosefile;
//
//    @FindBy(xpath = "//*[@id=\"edit-field-inline-pictures-source-und-form-field-img-news-original-und-0-upload-button\"]")
//    public WebElement pictureupload;
//
//    @FindBy(xpath = "//*[@id=\"edit-field-inline-pictures-source-und-form-actions-ief-add-save\"]")
//    public WebElement addpicture;
//
//    @FindBy(xpath = "//*[@id=\"edit-field-inline-pictures-source-und-form-actions-ief-add-cancel\"]")
//    public WebElement cancelpicture;


    //Keywords
    @FindBy(xpath = "//*[@id=\"edit-field-news-geography-und-0-field-geography-url-und-0-url\"]")
    public TextInput geography;

    @FindBy(xpath = "//*[@id=\"news-form-all-bundle\"]/div/div/div[11]/div/div[2]/ul/li[2]/a")
    public Button subjecttab;

    @FindBy(xpath = "//*[@id=\"edit-field-keywords-und\"]/ul/li[2]/ul/li[2]/div[2]/label")
    public CheckBox subject;

    @FindBy(xpath = "//*[@id=\"news-form-all-bundle\"]/div/div/div[11]/div/div[2]/ul/li[3]/a")
    public Button typemedia;

    @FindBy(xpath = "//*[@id=\"edit-field-media-type-tags-und-0-679-679-children-684-684-children-688-688\"]")
    public CheckBox typemediacheck;


    //Spotter Information
    @FindBy(xpath = "//*[@id=\"edit-field-spotter-urgency-und\"]")
    public Select spotterurgency;

    @FindBy(xpath = "//*[@id=\"edit-field-spotter-text-rating-und\"]")
    public Select storyrating;

    @FindBy(xpath = "//*[@id=\"edit-field-spotter-media-rating-und\"]")
    public Select picturerating;

    @FindBy(xpath = "//*[@id=\"edit-field-freelancer-name-und-0-value\"]")
    public TextInput name;

    @FindBy(xpath = "//*[@id=\"edit-field-freelancer-surname-und-0-value\"]")
    public TextInput surname;

    @FindBy(xpath = "//*[@id=\"edit-field-freelancer-email-und-0-value\"]")
    public TextInput email;

    @FindBy(xpath = "//*[@id=\"edit-field-freelancer-phone-und-0-value\"]")
    public TextInput phone;

    @FindBy(xpath = "//*[@id=\"edit-field-freelancer-country-und\"]")
    public TextInput spotterlocation;



    //Buttons
    @FindBy(xpath = "//*[@id=\"edit-field-freelancer-country-und\"]")
    public Button cancel;

    @FindBy(xpath = "//*[@id=\"news-form-all-bundle\"]/div/div/div[20]/div/div/button[2]")
    public Button storycontacts;

    @FindBy(xpath = "//*[@id=\"edit-submit-edit\"]")
    public Button saveandcontinue;

    public SubmitAsFreelancerPOM(WebDriver driver) {
        super(driver);
    }

    @Step
    public void submitNews() throws InterruptedException {

        GenerateData generateData = new GenerateData();

        String randname = generateData.generateRandomString(4);
        String randsurname = generateData.generateRandomString(6);

        mediatype.click();

        slug.clear();
        slug.sendKeys(generateData.generateRandomString(6));

        headline.sendKeys(generateData.generateRandomString(10));

        summary.sendKeys(generateData.generateRandomAlphaNumeric(20));

        story.sendKeys(generateData.generateRandomAlphaNumeric(30));

        legalnotes.sendKeys(generateData.generateRandomString(8));
        backgroundinfobutton.click();
        backgroundinfo.sendKeys(generateData.generateRandomString(8));
        alreadyoutbutton.click();
        wait.until(ExpectedConditions.elementToBeClickable((WebElement) alreadyout));
        alreadyout.sendKeys(generateData.generateRandomString(8));
        archivebutton.click();
        archive.sendKeys(generateData.generateRandomString(8));
        suggestsendingbutton.click();
        suggestsending.sendKeys(generateData.generateRandomString(8));

        geography.sendKeys(generateData.generateRandomString(10));
        subjecttab.click();
        subject.select();
        typemedia.click();
        typemediacheck.select();

        spotterurgency.selectByIndex(1);
        storyrating.selectByIndex(5);
        picturerating.selectByIndex(5);

        name.sendKeys(randname);
        surname.sendKeys(randsurname);
        email.sendKeys(randname + randsurname + "@yopmail.com");
        phone.sendKeys(generateData.generateRandomNumber(7));
        spotterlocation.sendKeys(generateData.generateRandomString(6));

        saveandcontinue.click();
    }

}
