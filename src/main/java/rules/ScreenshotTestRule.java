package rules;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.text.SimpleDateFormat;


public class ScreenshotTestRule  implements MethodRule {
    private static final Logger log = LogManager.getLogger(ScreenshotTestRule.class);

    public WebDriver driver;
    private SimpleDateFormat date=new SimpleDateFormat("dd_MM_yyyy_HH_mm");
    public ScreenshotTestRule(WebDriver driver) {
        this.driver = driver;
    }
    public Statement apply(final Statement statement, final FrameworkMethod frameworkMethod, final Object o) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    statement.evaluate();
                    driver.quit();
                    log.info(getClass().getSimpleName() + " test is done.");


                } catch (Throwable t) {
                    screenshot(frameworkMethod.getName());
                    throw t;
                }
            }
            @Attachment(type = "image/png")
            public byte[] screenshot(String fileName ) {
                try {
                    byte[] screenshot =  ((TakesScreenshot)driver ).getScreenshotAs(OutputType.BYTES);
                    log.error(getClass().getSimpleName() + " test is failed");
                    driver.quit();
                    return screenshot;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
    }

}